<?php

	include_once ('writeData.php');
	
	//getting xml file
	$url = 'SkierLogs.xml';
	$doc = new DOMDocument();
	$doc->load($url);

	$insert = new addData();
	
		//Getting Clubs
		
	// creating xpath
	$path = "/SkierLogs/Clubs/Club";	//Clubs path
	$xPath = new DOMXpath($doc);
	$clubs = $xPath->query($path);		// getting ClubList	
	
	$name   = NULL;
	$city   = NULL;
	$county = NULL;
	$node   = NULL;
	$res    = array();
	//Extract clubs
	foreach($clubs AS $club)
	{
		$clubId = $club->getAttribute('id');				//Get id
			$node = $club->getElementsByTagName('Name');	//Get name
		$name = $node->item(0)->textContent;
			$node = $club->getElementsByTagName('City');	//Get city
		$city = $node->item(0)->textContent;
			$node = $club->getElementsByTagName('County');	//Get county
		$county = $node->item(0)->textContent;
		
		$insert->addClubs($clubId, $name, $city, $county);	//Add to database
	}
	
	//Extract skiers
	$path = "/SkierLogs/Skiers/Skier";
	$skiers = $xPath->query($path);		

	$userName = NULL;
	$fName    = NULL;
	$lName    = NULL;
	$YOB      = NULL;
	$club     = NULL;
	$res      = array();
	
	
	foreach($skiers AS $skier)
	{
		$userName = $skier->getAttribute('userName');				//Get username	
			$node = $skier->getElementsByTagName('FirstName');		//Get firstName
		$fName = $node->item(0)->textContent;				
			$node = $skier->getElementsByTagName('LastName');		//Get lastName
		$lName = $node->item(0)->textContent;				
			$node = $skier->getElementsByTagName('YearOfBirth');	//Get year of birth
		$YOB = $node->item(0)->textContent;				
	
		$insert->addSkiers($userName, $fName, $lName, $YOB);		//Add to database
	}
	

$seasons=$xPath->query('/SkierLogs/Season');
	foreach($seasons AS $season) {
		$totalDistance = 0;
		$fallyear = $season->getAttribute('fallYear');				//Get fallYear
		$node1 = $season->getElementsByTagName('Skiers'); 
		foreach($node1 AS $seasonClub) {
			$clubId = $seasonClub->getAttribute('clubId');			//Get clubId
			$node2 = $seasonClub->getElementsByTagName('Skier');
			foreach($node2 AS $seasonUser) {
				$userName = $seasonUser->getAttribute('userName');	//Get userName			
				$node3 = $seasonUser->getElementsByTagName('Entry');

				foreach($node3 AS $entry) {
					$node = $entry->getElementsByTagName('Date');	//Get date
					$date = $node->item(0)->textContent;
					
					$node = $entry->getElementsByTagName('Area');	//Get area
					$area = $node->item(0)->textContent;
					
					$node=$entry->getElementsByTagName('Distance');	//Get distance
					$distance = $node->item(0)->textContent;
					$totalDistance += $distance;					//Add up total distance
					
					$insert->addEntry($date, $area, $distance);		//Add to database
					

					
				}
			$insert->addSeason($fallyear, $clubId, $userName, $totalDistance);	//Add to database
			$totalDistance = 0;
			}


		}
		

	}
	
	
	
	
	
	
	
	
	

?>