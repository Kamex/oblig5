<?php

class addData {
	// Create PDO connection
	 function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
				$this->db = new PDO('mysql:host=localhost;dbname=assignment5;charset=utf8', 'root', '');		
		}
	}
            
//add skiers to database¨
     function addSkiers($userName, $fName, $lName, $YOB) {
        $stmt = $this->db->prepare("INSERT INTO skiers (userName, fName, lName, yearOfBirth) 
				VALUES (:userName, :lName, :fName, :yearOfBirth)");
				
		$stmt->bindValue(':userName',    $userName);
        $stmt->bindValue(':fName',   $fName);
        $stmt->bindValue(':lName',    $lName);
        $stmt->bindValue(':yearOfBirth', $YOB);
        $stmt->execute();
    }
	
//add clubs to database
     function addClubs($clubId, $name, $city, $county) {
        $stmt = $this->db->prepare("INSERT INTO clubs (clubId, name, city, county) 
				VALUES (:clubId, :name, :city, :county)");
				
		$stmt->bindValue(':clubId',    $clubId);
        $stmt->bindValue(':name',   $name);
        $stmt->bindValue(':city',    $city);
        $stmt->bindValue(':county', $county);
        $stmt->execute();
    }
//Add season to database
     function addSeason($fallYear, $clubId, $userName, $totalDistance) {
        $stmt = $this->db->prepare("INSERT INTO season (fallYear, clubId, userName, totalDistance) 
				VALUES (:fallYear, :clubId, :userName, :totalDistance)");
				
		$stmt->bindValue(':fallYear',    $fallYear);
        $stmt->bindValue(':clubId',   $clubId);
        $stmt->bindValue(':userName',    $userName);
		$stmt->bindValue(':totalDistance',    $totalDistance);
        $stmt->execute();
    }

//Add entry to database
     function addEntry($date, $area, $distance) {
        $stmt = $this->db->prepare("INSERT INTO entry (date, area, distance) 
				VALUES (:date, :area, :distance)");
				
        $stmt->bindValue(':date',   $date);
		$stmt->bindValue(':area',    $area);
        $stmt->bindValue(':distance',    $distance);
        $stmt->execute();
    }

}
?>